package testApplication.repository;

import testApplication.exception.UserNotFoundException;
import testApplication.model.User;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * UserRepositoryImpl.
 */
public class UserRepositoryImpl extends SimpleJpaRepository<User, Long> implements UserRepository {

    private EntityManager entityManager;

    /**
     * UserRepository constructor.
     * @param em entity manager
     */
    public UserRepositoryImpl(EntityManager em) {
        super(User.class, em);
        this.entityManager = em;
    }

    /**
     * Get user by id
     * @param id user id
     * @return Optional
     */
    public Optional<User> getUserById(Long id) {
        TypedQuery<User> typedQuery = this.entityManager.createQuery("from User where id='"+ id +"'", User.class);
        return Optional.of(typedQuery.getSingleResult());
    }

    /**
     * Get user by username
     * @param username username
     * @return User
     */
    public User getUserByUsername(String username) {
        TypedQuery<User> typedQuery = this.entityManager.createQuery("from User where username LIKE '"+ username +"'", User.class);
        return typedQuery.getSingleResult();
    }

    /**
     * Get all users
     * @return List
     */
    public List<User> getAllUsers() {
        TypedQuery<User> typedQuery = this.entityManager.createQuery("from User order by id", User.class);
        return typedQuery.getResultList();
    }

    /**
     * Create new user
     * @param userRequest json with user properties
     * @return User
     */
    @Transactional
    public User createUser(User userRequest) {
        User user = new User();
        user.setName(userRequest.getName());
        user.setUsername(userRequest.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(userRequest.getPassword()));
        return this.save(user);
    }

    /**
     * Update existing user
     * @param id user id
     * @param userRequest json with user properties
     * @return User
     */
    @Transactional
    public User updateUser(Long id, User userRequest) {
        Optional<User> userEntity = getUserById(id);
        return getUserById(id).map(user -> {
            user.setName(userRequest.getName());
            user.setUsername(userRequest.getUsername());
            if (userRequest.getPassword().isEmpty())
                user.setPassword(userEntity.get().getPassword());
            return this.save(user);
        }).orElseThrow(() -> new UserNotFoundException("User not found with id " + id));
    }

    /**
     * Delete existing user
     * @param id user id
     * @return ResponseEntity
     */
    @Transactional
    public ResponseEntity<?> deleteUser(Long id) {
        return getUserById(id).map(user -> {
            this.delete(user);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new UserNotFoundException("User not found with id " + id));
    }
}