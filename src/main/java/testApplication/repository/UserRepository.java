package testApplication.repository;

import testApplication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * UserRepository.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public Optional<User> getUserById(Long id);
    public User getUserByUsername(String username);
    public List<User> getAllUsers();
    public User createUser(User userRequest);
    public User updateUser(Long id, User userRequest);
    public ResponseEntity<?> deleteUser(Long id);
}