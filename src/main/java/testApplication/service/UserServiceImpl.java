package testApplication.service;

import java.util.List;
import java.util.Optional;

import testApplication.model.User;
import testApplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * UserServiceImpl
 */
@Service
public class UserServiceImpl implements UserService {
    
    private UserRepository userRepository;

    /**
     * @param userRepository
     */
    @Autowired(required = true)
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Create new user
     * @param user user
     * @return User
     */
    @Transactional
    public User createUser(User user) {
        return this.userRepository.createUser(user);
    }

    /**
     * Update existing user
     * @param id user id
     * @param user user
     * @return User
     */
    @Transactional
    public User updateUser(Long id, User user) {
        return this.userRepository.updateUser(id, user);
    }

    /**
     * Get all users
     * @return List
     */
    @Transactional
    public List<User> getAllUsers() {
        return this.userRepository.getAllUsers();
    }

    /**
     * Get user by id
     * @param id user id
     * @return Optional
     */
    @Transactional
    public Optional<User> getUserById(Long id) {
        return this.userRepository.getUserById(id);
    }

    /**
     * Delete existing user
     * @param id user id
     * @return ResponseEntity
     */
    @Transactional
    public ResponseEntity<?> deleteUser(Long id) {
        return this.userRepository.deleteUser(id);
    }

}

