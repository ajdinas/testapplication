package testApplication.service;

import testApplication.model.User;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

/**
 * UserService.
 */
public interface UserService {
    public User createUser(User user);
    public User updateUser(Long id, User user);
    public List<User> getAllUsers();
    public Optional<User> getUserById(Long id);
    public ResponseEntity<?> deleteUser(Long id);
}
