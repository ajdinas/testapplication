package testApplication;

import testApplication.model.User;
import testApplication.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	/**
	 * Insert testApplication users to db
	 * @param userRepository UserRepository
	 * @return CommandLineRunner
	 */
	@Bean
	CommandLineRunner init (UserRepository userRepository){
		return args -> {
			BCryptPasswordEncoder e = new BCryptPasswordEncoder();

			User user1 = new User();
			user1.setName("bill");
			user1.setUsername("user1");
			user1.setPassword(e.encode("thisisbill"));
			userRepository.save(user1);

			User user2 = new User();
			user2.setName("joe");
			user2.setUsername("user2");
			user2.setPassword(e.encode("thisisjoe"));
			userRepository.save(user2);
		};
	}
}