package testApplication.controller;

import testApplication.MvcConfiguration;
import testApplication.exception.UserNotFoundException;
import testApplication.model.User;
import testApplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * User Controller.
 */
@RestController
@RequestMapping("/api")
public class UserController extends MvcConfiguration {

    private UserService userService;

    /**
     * User service
     * @param userService user service
     */
    @Autowired(required = true)
    public void setUserService(UserService userService){
        this.userService = userService;
    }

    /**
     * Get all users
     * @return List
     */
    @GetMapping("/users")
    public List<User> getUsers() {
        return userService.getAllUsers();
    }

    /**
     * Get user with id userId
     * @param userId user id
     * @return User
     * @throws Exception
     */
    @GetMapping("/users/{userId}")
    public User getUser(@PathVariable Long userId) throws Exception {
        try {
            return userService.getUserById(userId).get();
        } catch (Exception e) {
            throw new UserNotFoundException("User not found with id " + userId);
        }
    }

    /**
     * Create new user
     * @param userRequest json with user properties
     * @return User
     */
    @PostMapping("/users")
    public User createUser(@Valid @RequestBody User userRequest) {
        return userService.createUser(userRequest);
    }

    /**
     * Edit existing user with id userId
     * @param userId user id
     * @param userRequest json with user properties
     * @return User
     */
    @PutMapping("/users/{userId}")
    public User updateUser(@PathVariable Long userId, @Valid @RequestBody User userRequest) {
        return userService.updateUser(userId, userRequest);
    }

    /**
     * Delete existing user with id userId
     * @param userId user id
     * @return ResponseEntity
     */
    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        return userService.deleteUser(userId);
    }
}