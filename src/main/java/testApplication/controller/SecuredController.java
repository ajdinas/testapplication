package testApplication.controller;

import testApplication.MvcConfiguration;
import testApplication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Secured Controller.
 */
@Controller
public class SecuredController extends MvcConfiguration {

    private UserService userService;

    /**
     * User service
     * @param userService user service
     */
    @Autowired(required = true)
    public void setUserService(UserService userService){
        this.userService = userService;
    }

    /**
     * Delete user with id userId
     * @param userId user id
     * @return ResponseEntity
     */
    @DeleteMapping("/secured/deleteUser/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        return userService.deleteUser(userId);
    }
}
