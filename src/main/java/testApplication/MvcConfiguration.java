package testApplication;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * MvcConfiguration.
 */
@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    /**
     * Ad views to controllers
     * @param registry
     */
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/secured/deleteUser").setViewName("deleteUser");
    }
}