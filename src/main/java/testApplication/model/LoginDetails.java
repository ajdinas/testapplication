package testApplication.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * LoginDetails.
 */
public class LoginDetails implements UserDetails {

    private User user;

    /**
     * LoginDetails constructor.
     * @param user user
     */
    public LoginDetails(User user) {
        this.user = user;
    }

    /**
     * Get username
     * @return String
     */
    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    /**
     * Get password
     * @return String
     */
    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}