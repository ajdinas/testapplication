package testApplication.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * User.
 */
@Entity
@Table(name = "users")
public class User {
    /**
     * Id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    /**
     * Name
     */
    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    /**
     * Username
     */
    @NotBlank
    @Size(min = 3, max = 100)
    private String username;

    /**
     * Password
     */
    private String password;

    /**
     * Get id
     * @return Long
     */
    public Long getId() {
        return id;
    }

    /**
     * Get name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Set name
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get username
     * @return String
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set username
     * @param username username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get password
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set password
     * @param password password
     */
    public void setPassword(String password) {
        this.password = password;
    }
}