import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Table, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Request from "superagent";

class Users extends React.Component {

    constructor(props) {
        super(props);
        this.state = {users: [], newUser: {"name": "", "username": "", "password": ""}, nextId: 0};
    }

    componentDidMount() {
        this.getUsers();
    }

    getUsers() {
        this.setState({isLoading: true});
        fetch('api/users')
            .then(response => response.json()).then((data) => {
                this.setState({users: data, isLoading: false});
                this.updateNextId();
        })
    }

    resetNewUser() {
        this.setState({
            newUser: {
                "name": "",
                "username": "",
                "password": ""
            }
        });
    }

    updateNextId() {
        var nextId = this.state.users.slice(-1)[0] === undefined ? 1 : this.state.users.slice(-1)[0].id + 1;
        this.setState({nextId: nextId});
    }

    handleRowEdit(user) {
        Request.put('/api/users/' + user.id)
            .send({
                "name": user.name,
                "username": user.username,
                "password": ""
            })
            .set('Accept', 'application/json')
            .then(() => {
                this.getUsers();
            })
            .catch(err => {
                alert('ERROR: Wrong input. User was not edited.');
            });
    }

    handleRowDelete(user) {
        Request.delete('/secured/deleteUser/' + user.id)
            .set('Accept', 'application/json')
            .then(() => {
                this.getUsers();
            })
            .catch(err => {
                alert('ERROR: User was not deleted.');
            });
    }

    handleUserTable(evt) {
        var item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
        var users = this.state.users.slice();
        var newUsers = users.map(function(user) {

            for (var key in user) {
                if (key == item.name && user.id == item.id) {
                    user[key] = item.value;

                }
            }
            return user;
        });
        this.setState({users:newUsers});
    };

    handleTextFieldUpdate(evt) {
        console.log(evt.target.name);
        console.log(evt.target.value);
        var item = {
            name: evt.target.name,
            value: evt.target.value
        };

        var newUser = this.state.newUser;

        for (var key in this.state.newUser) {
            if (key == item.name) {
                newUser[key] = item.value;
            }
        }

        this.setState({newUser: newUser});
    }

    handleAddForm(user) {
        console.log(user);
        Request.post('/api/users/')
            .send({
                "name": user.name,
                "username": user.username,
                "password": user.password
            })
            .set('Accept', 'application/json')
            .then(() => {
                this.resetNewUser();
                this.getUsers();
            })
            .catch(err => {
                alert('ERROR: Wrong input. User was not added.');
            });
    }

    render() {
        const userTable = <UserTable onUserTableUpdate={this.handleUserTable.bind(this)} onRowEdit={this.handleRowEdit.bind(this)} onRowDelete={this.handleRowDelete.bind(this)} users={this.state.users}/>;
        const userForm = <TextField onAddEvent={this.handleAddForm.bind(this)} onTextFieldUpdate={this.handleTextFieldUpdate.bind(this)} newUser={this.state.newUser}/>;
        return (
            <Container>
                <br/>
                {userTable}
                {userForm}
            </Container>
        );
    }
}

class UserTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var onUserTableUpdate = this.props.onUserTableUpdate;
        var rowEdit = this.props.onRowEdit;
        var rowDelete = this.props.onRowDelete;
        var user = this.props.users.map(function(user) {
            return (<UserRow onUserTableUpdate={onUserTableUpdate} user={user} onEditEvent={rowEdit.bind(this)} onDeleteEvent={rowDelete.bind(this)} key={user.id}/>)
        });
        return (
            <div>
                <h2>List of users</h2>
                <Table className="table table-bordered table-striped">
                    <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                        {user}
                    </tbody>
                </Table>
            </div>
        );
    }
}

class UserRow extends React.Component {
    onEditEvent() {
        this.props.onEditEvent(this.props.user);
    }

    onDeleteEvent() {
        this.props.onDeleteEvent(this.props.user);
    }

    render() {
        return (
            <tr>
                <td>{this.props.user.id}</td>
                <EditableCell onUserTableUpdate={this.props.onUserTableUpdate} cellData={{
                    "type": "name",
                    value: this.props.user.name,
                    id: this.props.user.id
                }}/>
                <EditableCell onUserTableUpdate={this.props.onUserTableUpdate} cellData={{
                    "type": "username",
                    value: this.props.user.username,
                    id: this.props.user.id
                }}/>
                <td className="del-cell">
                    <Button type="button" onClick={this.onEditEvent.bind(this)} className="btn btn-info">Save</Button>
                    &nbsp;
                    <Button type="button" onClick={this.onDeleteEvent.bind(this)} className="btn btn-danger">Delete</Button>
                </td>
            </tr>
        );
    }
}

class EditableCell extends React.Component {
    render() {
        return (
            <td>
                <input type='text' name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onUserTableUpdate} className="form-control"/>
            </td>
        );
    }
}

class TextField extends React.Component {
    onAddEvent() {
        this.props.onAddEvent(this.props.newUser);
    }

    render() {
        return (
            <div>
                <h2>Add user</h2>
                <Table className="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <td>
                                <input type='text' name="name" placeholder="Name" value={this.props.newUser.name} onChange={this.props.onTextFieldUpdate} className="form-control"/>
                            </td>
                            <td>
                                <input type='text' name="username" placeholder="Username" value={this.props.newUser.username} onChange={this.props.onTextFieldUpdate} className="form-control"/>
                            </td>
                            <td>
                                <input type='text' name="password" placeholder="Password" value={this.props.newUser.password} onChange={this.props.onTextFieldUpdate} className="form-control"/>
                            </td>
                            <td>
                                <Button onClick={this.onAddEvent.bind(this)} className="btn btn-success">Add</Button>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        );
    }
}

UserTable.propTypes = {
    users: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            username: PropTypes.string.isRequired,
            password: PropTypes.string.isRequired
        })
    ).isRequired
};

ReactDOM.render(
    <Users />,
    document.getElementById('react')
);